%bcond_with tests

Name:           python-requests
Version:        2.27.1
Release:        1
Summary:        Python HTTP Library
License:        ASL 2.0
URL:            http://python-requests.org/
Source0:        https://github.com/psf/requests/archive/refs/tags/v%{version}.tar.gz

Patch6001:      backport-requests-2.12.4-tests_nonet.patch
Patch6002:      backport-patch-requests-certs.py-to-use-the-system-CA-bundle.patch
Patch6003:      backport-remove-tests-that-use-the-tarpit.patch

BuildArch:      noarch

%description
Requests is an HTTP library, written in Python, as an alternative
to Python's builtin urllib2 which requires work (even
method overrides) to perform basic tasks.
Features of Requests:
 - GET, HEAD, POST, PUT, DELETE Requests:
   + HTTP Header Request Attachment.
   + Data/Params Request Attachment.
   + Multipart File Uploads.
   + CookieJar Support.
   + Redirection History.
   + Redirection Recursion Urllib Fix.
   + Automatic Decompression of GZipped Content.
   + Unicode URL Support.
 - Authentication:
   + URL + HTTP Auth Registry.

%package -n      python%{python3_pkgversion}-requests
Summary:         Awesome Python HTTP Library That's Actually Usable

%{?python_provide:%python_provide python%{python3_pkgversion}-requests}

BuildRequires:   python%{python3_pkgversion}-devel
BuildRequires:   python%{python3_pkgversion}-chardet
BuildRequires:   python%{python3_pkgversion}-urllib3
BuildRequires:   python%{python3_pkgversion}-idna

Provides:        python%{python3_pkgversion}dist(requests) = %{version}
Provides:        python%{python3_version}dist(requests) = %{version}

%if %{with tests}
BuildRequires:   python%{python3_pkgversion}-pytest
BuildRequires:   python%{python3_pkgversion}-pytest-cov
BuildRequires:   python%{python3_pkgversion}-pytest-httpbin
BuildRequires:   python%{python3_pkgversion}-pytest-mock
%endif

Requires:        python%{python3_pkgversion}-chardet
Requires:        python%{python3_pkgversion}-urllib3
Requires:        python%{python3_pkgversion}-idna
Requires:        ca-certificates
Requires:        python%{python3_pkgversion}-charset-normalizer

%description -n  python%{python3_pkgversion}-requests
Requests is an ISC Licensed HTTP library, written in Python, for human
beings.
Most existing Python modules for sending HTTP requests are extremely
verbose and cumbersome. Python's builtin urllib2 module provides most of
the HTTP capabilities you should need, but the api is thoroughly broken.
It requires an enormous amount of work (even method overrides) to
perform the simplest of tasks.

%package_help

%prep
%autosetup -p1 -n requests-%{version}
rm -rf requests/cacert.pem
sed -i '/#!\/usr\/.*python/d' requests/certs.py

%build
%py3_build

%install
%py3_install

%if %{with tests}
%check
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} -m pytest -v
%endif 

%files -n python%{python3_pkgversion}-requests 
%defattr(-,root,root)
%license LICENSE
%{python3_sitelib}/*.egg-info/*
%{python3_sitelib}/requests/

%files    help
%defattr(-,root,root)
%doc HISTORY.md README.md

%changelog
* Thu Jun 16 2022 zhangying <zhangy1317@foxmail.com> - 2.27.1-1
- Upgrade version for openstack yoga

* Wed Dec 22 2021 wangkerong <wangkerong@huawei.com> - 2.26.0-3
- add python-charset-normalizer requires

* Thu Dec 09 2021 wangkerong <wangkerong@huawei.com> - 2.26.0-2
- provide pythonxdist(requests)

* Sat Dec 04 2021 wangkerong <wangkerong@huawei.com> - 2.26.0-1
- update to 2.26.0

* Thu Feb 4 2021 chengguipeng <chengguipeng1@huawei.com> - 2.25.1-1
- update to 2.25.1

* Wed Oct 14 2020 shangyibin <shangyibin1@huawei.com> - 2.24.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: remove python2-requests package

*Tue Aug 4 2020 wuchaochao <wuchaochao4@huawei.com> - 2.24.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: remove reliance on  "certifi" 

* Thu Jul 23 2020 wuchaochao <wuchaochao4@huawei.com> - 2.24.0-1
- update package to 2.24.0

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.21.0-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete describe info

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.21.0-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the patch

* Wed Oct 30 2019 hexiaowen <hexiaowen@huawei.com> - 2.21.0-3
- disable tests

* Thu Oct 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.21.0-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the provides of python3

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.21.0-1
- Package init
